package com.conduent.databroker.kafka_maven.model;

public class EventHitModel {
//    private long transactionId;
//    private long clientId;
//    private long gantryId;
//    private long dateTime;
    private String direction;
    private String callback;
    //private String authToken;


    public EventHitModel() {
    }
//
//    public EventHitModel(long transactionId, long clientId, long gantryId, long dateTime, String direction, String callback, String authToken) {
//        this.transactionId = transactionId;
//        this.clientId = clientId;
//        this.gantryId = gantryId;
//        this.dateTime = dateTime;
//        this.direction = direction;
//        this.callback = callback;
//        this.authToken = authToken;
//    }
//
//    public long getTransactionId() {
//        return transactionId;
//    }
//
//    public void setTransactionId(long transactionId) {
//        this.transactionId = transactionId;
//    }
//
//    public long getClientId() {
//        return clientId;
//    }
//
//    public void setClientId(long clientId) {
//        this.clientId = clientId;
//    }
//
//    public long getGantryId() {
//        return gantryId;
//    }
//
//    public void setGantryId(long gantryId) {
//        this.gantryId = gantryId;
//    }
//
//    public long getDateTime() {
//        return dateTime;
//    }
//
//    public void setDateTime(long dateTime) {
//        this.dateTime = dateTime;
//    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

//    public String getAuthToken() {
//        return authToken;
//    }
//
//    public void setAuthToken(String authToken) {
//        this.authToken = authToken;
//
//    }

//    public EventHitModel(String direction, String callback, String authToken) {
//        this.direction = direction;
//        this.callback = callback;
//        this.authToken = authToken;
//    }
    //    @Override
//    public String toString() {
//        return "EventHitModel{" +
//                "transactionId=" + transactionId +
//                ", clientId=" + clientId +
//                ", gantryId=" + gantryId +
//                ", dateTime=" + dateTime +
//                ", direction='" + direction + '\'' +
//                ", callback='" + callback + '\'' +
//                ", authToken='" + authToken + '\'' +
//                '}';


    public EventHitModel(String direction, String callback) {
        this.direction = direction;
        this.callback = callback;
    }
}

