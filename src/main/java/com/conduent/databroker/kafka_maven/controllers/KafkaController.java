package com.conduent.databroker.kafka_maven.controllers;

import com.conduent.databroker.kafka_maven.KafkaSender;
import com.conduent.databroker.kafka_maven.model.EventHitModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

import org.springframework.web.bind.annotation.*;

@RestController
public class KafkaController {

    @Autowired
    private KafkaTemplate<String, EventHitModel> kafkaTemplate;

    @PostMapping("/send")
        public void enqueueEventHit(@RequestBody EventHitModel model) {
        kafkaTemplate.send("EventHitInput", model);
        System.out.println("Message: "+model+" sent to topic: "+"EventHitInput");

    }
}
